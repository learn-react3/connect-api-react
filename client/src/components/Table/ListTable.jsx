import React from "react";

import LinkButton from "../Button/LinkButton";

const ListTable = (props) => {
  return (
    <>
      <h2 className="mt-4">List Pokemon</h2>
      <div className="mt-2 style-table">
        <table className="table mt-3">
          <thead>
            <tr>
              <th>Name</th>
              <th>Detail</th>
            </tr>
          </thead>
          <tbody>
            {props.list.map((item, index) => (
              <tr key={index}>
                <td>{item.name}</td>
                <td>
                  <LinkButton
                    className="btn btn-success"
                    linkTo="Detail"
                    name="Detail"
                    handleClick={() => props.setUrlDetail(item.url)}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default ListTable;
