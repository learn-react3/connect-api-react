import React from "react";

// import components
import ImagePoke from "../ImagePoke/Image";
import LinkButton from "../Button/LinkButton";

const DetailTable = (props) => {
  return (
    <>
      <h2 className="mt-4">Detail Pokemon</h2>
      <div className="mt-2 style-table row">
        <ImagePoke src={props.imagePokemon} />
        <ul
          className="table mt-3 col-5 offset-1"
          style={{ fontSize: "36px", listStyle: "none" }}
        >
          <li>name : {props.detail.name}</li>
          <li>height : {props.detail.height} ft</li>
          <li>weight : {props.detail.weight} pound</li>
          <LinkButton
            name="Back to List"
            className="btn btn-warning"
            linkTo=""
          />
        </ul>
      </div>
    </>
  );
};

export default DetailTable;
