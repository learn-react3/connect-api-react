import React from "react";

const ImagePoke = (props) => {
  return (
    <>
      <img src={props.src} alt="" style={{ width: "300px", height: "300px" }} />
    </>
  );
};

export default ImagePoke;
