import React from "react";
import { Link } from "react-router-dom";

const LinkButton = (props) => {
  return (
    <>
      <Link
        className={`${props.className} mx-1`}
        to={`/${props.linkTo}`}
        onClick={props.handleClick}
      >
        {props.name}
      </Link>
    </>
  );
};

export default LinkButton;
