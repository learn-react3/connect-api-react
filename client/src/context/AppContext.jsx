import React, { createContext, useEffect, useState } from "react";
import axios from "axios";

// export context
export const ListContext = createContext({});
export const DetailContext = createContext({});

const AppContext = ({ children }) => {
  // state
  const [list, setList] = useState([]);
  const [urlDetail, setUrlDetail] = useState([]);

  // const getList = async () => {
  //   try {
  //     const response = await fetch(
  //       "https://pokeapi.co/api/v2/pokemon?limit=100&offset=200",
  //       {
  //         method: "GET",
  //       }
  //     );

  //     console.log(response);

  //     const pokeList = await response.json();
  //     console.log(pokeList.results);

  //     setList(pokeList.results);
  //   } catch (error) {}
  // };

  useEffect(() => {
    // getList();
    axios
      .get("https://pokeapi.co/api/v2/pokemon?limit=100&offset=200")
      .then((res) => {
        setList(res.data.results);
        console.log(res.data.results);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <ListContext.Provider value={{ list, setList }}>
      <DetailContext.Provider value={{ urlDetail, setUrlDetail }}>
        {children}
      </DetailContext.Provider>
    </ListContext.Provider>
  );
};

export default AppContext;
