import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// import context
import ListContext from "./context/AppContext";

// import component
import List from "./pages/List";
import DetailList from "./pages/Detail";

function App() {
  return (
    <div className="container">
      <ListContext>
        <Router>
          <Switch>
            <Route exact path="/" component={List} />
            <Route path="/Detail" component={DetailList} />
          </Switch>
        </Router>
      </ListContext>
    </div>
  );
}

export default App;
