import React, { useContext } from "react";

// import component
import ListTable from "../components/Table/ListTable";

// import context
import { ListContext } from "../context/AppContext";
import { DetailContext } from "../context/AppContext";

const List = () => {
  const { list } = useContext(ListContext);
  const { setUrlDetail } = useContext(DetailContext);

  return (
    <>
      <ListTable list={list} setUrlDetail={setUrlDetail} />
    </>
  );
};

export default List;
