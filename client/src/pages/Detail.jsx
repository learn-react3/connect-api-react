import React, { useEffect, useContext, useState } from "react";
import axios from "axios";

// import component
import DetailTable from "../components/Table/DetailTable";

// import context
import { DetailContext } from "../context/AppContext";

const Detail = () => {
  // state
  const { urlDetail } = useContext(DetailContext);
  const [detail, setDetail] = useState({});
  const [imagePokemon, setImagePokemon] = useState("");

  useEffect(() => {
    axios
      .get(`${urlDetail}`)
      .then((res) => {
        console.log(res.data);
        setImagePokemon(res.data.sprites.front_shiny);
        setDetail(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [urlDetail]);

  return (
    <>
      <DetailTable detail={detail} imagePokemon={imagePokemon} />
    </>
  );
};

export default Detail;
